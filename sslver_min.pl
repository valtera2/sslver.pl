#!/usr/bin/perl
#this version does not require PerlIO.pm but lacks error detail in output.
my @ver = qw (ssl3 tls1 tls1_1 tls1_2);
#my @ver = qw (ssl3);
#host must be with port
print "enter hostname and port: \n";
chomp(my $host=<STDIN>);
foreach $ssl_ver (@ver) {
	print "Testing $ssl_ver ...\n";
	$command = "echo -n | openssl s_client -$ssl_ver -connect $host";
	$command .= " 2>&1";
	$_ = `$command`;
	if (/error/) {
	print "NO \n";
	}
	elsif (/Cipher is/) {
		print "YES \n";
	}
	else {print "UNKNOWN RESPONSE \n";
	}
}
exit;
