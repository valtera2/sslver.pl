#!/usr/bin/perl
my @ver = qw (ssl3 tls1 tls1_1 tls1_2);
#host must be with port
print "enter hostname and port: \n";
chomp(my $host=<STDIN>);
foreach $ssl_ver (@ver) {
	print "Testing $ssl_ver ...\n";
	$command = "echo -n | openssl s_client -$ssl_ver -connect $host";
	$command .= " 2>&1";
	$_ = `$command`;
	if (/error/) {
		$errors = $_;
		open my $fh, "<", \$errors
			or die "could not open inmemory file";
			while (my $line = <$fh>) {
				if ($. == 5) {
					$error = $line;
					chomp($error);
				}
			}
		
		print " NO $error \n";
	}
	elsif (/Cipher is/) {
		print "YES \n";
	}
	else {print "UNKNOWN RESPONSE \n";
	}
}
exit;
